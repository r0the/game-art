# Fish

All artwork in this directory has been released under a [Public Domain](http://creativecommons.org/publicdomain/zero/1.0/) licence.

Author:
- Fish: [Kenney](https://opengameart.org/users/kenney)

Sources:
- [OpenGameArt.Org: Puzzle game art](https://opengameart.org/content/puzzle-game-art)
