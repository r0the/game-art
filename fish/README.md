# Fish

All artwork in this directory has been released under a [Public Domain](http://creativecommons.org/publicdomain/zero/1.0/) licence.

Authors:
- Fish: [Kenney](https://opengameart.org/users/kenney)
- Underwater scene: [Scribe](https://opengameart.org/users/scribe)

Sources:
- [OpenGameArt.Org: Underwater scene (loopable)](http://opengameart.org/content/underwater-scene-loopable)
- [OpenGameArt.Org: Fish Pack](https://opengameart.org/content/fish-pack-0)
